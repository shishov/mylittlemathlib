/* Copyright 2013 Boris Shishov */

#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include <errno.h> 
#include <assert.h>

#define MIN(X,Y) ((X) < (Y) ? (X) : (Y))
#define MAX(X,Y) ((X) < (Y) ? (Y) : (X))
#define NEWM(cols,rows) (*(new Matrix(cols, rows)))

typedef unsigned int uint;

// My Little Math Lib NameSpace
namespace mlml{
	class Matrix;	

	Matrix& Copy(Matrix& m);	
	void Resize(Matrix& m, uint cols, uint rows);
	void Clear(Matrix& m);
	void Print(Matrix& m);
	bool Equal(Matrix& a, Matrix& b);
	
	// Basic Operations
	Matrix& Mul(Matrix& a, Matrix& b);
	Matrix& Mul(Matrix& a, double b);
	Matrix& Div(Matrix& a, double b);
	Matrix& Add(Matrix& a, Matrix& b);
	Matrix& Add(Matrix& a, double b);
	Matrix& Sub(Matrix& a, Matrix& b);
	Matrix& Sub(Matrix& a, double b);

	// Math
	uint Length(Matrix& m);
	uint Rang(Matrix& m);
	double Det(Matrix& m);	
	double Minor(Matrix& m, uint row, uint col);
	double Cofactor(Matrix& m, uint i, uint j);	
	
	void Transpose(Matrix& m);	
	void SwapRows(Matrix& m, uint row1, uint row2);
	void SwapCols(Matrix& m, uint col1, uint col2);
	void SumRows(Matrix& m, uint from, uint to, double k = 1);
	void SumCols(Matrix& m, uint from, uint to, double k = 1);	
	void MultiplyRow(Matrix& m, uint i, double k);

	void SelfMultiply(Matrix& m, double b);

	Matrix& Slice(Matrix& m, uint row, uint col);
	Matrix& Adjugate(Matrix& m);
	Matrix& Inverse(Matrix& m);
	Matrix& Triangular(Matrix& m);
	
	class Matrix 
	{
	private:
		uint cols;
		uint rows;
		double** data;

	public:
		Matrix(uint cols, uint rows) {
			this->cols = cols;
			this->rows = rows;
			this->data = (double**) calloc(cols * rows, sizeof(double));

			assert(this != NULL);

			for(uint i = 0; i < this->rows; i++)	
				this->data[i] = (double*) calloc(cols, sizeof(double));
		}

		Matrix(uint size) {
			Matrix(size, size);
		}

		~Matrix() {
			delete[] data;
		}

		inline const int Rows() {return this->rows;}
		inline const int Cols() {return this->cols;}
		void Resize(uint cols, uint rows){
			delete [] data;
			Matrix(cols,rows);
		}		

		double* operator[] (const uint i)						{return this->data[i];}
		double& operator() (const uint row, const uint col)		{return this->data[row][col];}
		inline friend Matrix& operator+ (Matrix& a, Matrix& b)	{return Add(a,b);}
		inline friend Matrix& operator+ (Matrix& a, double b)	{return Add(a,b);}		
		inline friend Matrix& operator- (Matrix& a, Matrix& b)	{return Sub(a,b);}
		inline friend Matrix& operator- (Matrix& a, double b)	{return Sub(a,b);}
		inline friend Matrix& operator* (Matrix& a, Matrix& b)	{return Mul(a,b);}
		inline friend Matrix& operator* (Matrix&a, double b)	{return Mul(a,b);}		
		inline friend Matrix& operator/ (Matrix&a, double b)	{return Div(a,b);}
		inline Matrix& operator! ()								{return Inverse(*this);}		
		inline bool operator== (Matrix& b)						{return  Equal(*this, b);}
		inline bool operator!= (Matrix& b)						{return !Equal(*this, b);}		
	};

	Matrix& Copy(Matrix& m) {
		Matrix& res = NEWM(m.Cols(),m.Rows());

		for(uint i = 0; i < m.Rows(); i++)
			for(uint j = 0; j < m.Cols(); j++)		
				res[i][j] = m[i][j];

		return res;
	};

	void Print(Matrix& m){
		for(uint i = 0; i < m.Rows(); i++)
		{
			for(uint j = 0; j < m.Cols(); j++)				
				printf("%f ", m[i][j]);					

			printf("\n");		
		}
		printf("\n");	
	}

	void Clear(Matrix& m){
		for(uint i = 0; i < m.Rows(); i++)
			for(uint j = 0; j < m.Cols(); j++)
				m[i][j] = 0;
	}

	void Resize(Matrix& m, uint cols, uint rows){
		m.Resize(cols, rows);
	}

	uint Length(Matrix& m) {
		return m.Cols() * m.Rows();
	}

	bool Equal(Matrix& a, Matrix& b)
	{
		if(a.Rows() != b.Rows() || a.Cols() != b.Cols())
			return false;

		for(uint i = 0; i < a.Rows(); i++)
			for(uint j = 0; j < a.Cols(); j++)
				if(a[i][j] != b[i][j]) return false;

		return true;
	}

#pragma region Basic Operations

	Matrix& Mul(Matrix& a, Matrix& b)
	{
		assert(a.Cols() == b.Rows());

		Matrix& c = NEWM(a.Rows(),b.Cols());
		uint n = a.Cols();

		for(uint i = 0; i < a.Rows(); i++)		
			for(uint j = 0; j < b.Cols(); j++)		
			{
				double sum = 0;

				for(uint k = 0; k < n; k++)		
					sum += a[i][k] * b[k][j];

				c[i][j] = sum;
			}

			return c;
	}

	Matrix& Mul(Matrix& a, double b){
		Matrix& res = NEWM(a.Cols(), a.Rows());

		for(uint i = 0 ; i < a.Rows(); i++)
			for(uint j = 0; j < a.Cols(); j++)		
				res[i][j] = a[i][j] * b;

		return res;
	}	

	Matrix& Div(Matrix& a, double b){
		Matrix& res = NEWM(a.Cols(), a.Rows());

		for(uint i = 0 ; i < a.Rows(); i++)
			for(uint j = 0; j < a.Cols(); j++)		
				res[i][j] = a[i][j] / b;

		return res;
	}

	Matrix& Add(Matrix& a, double b){
		Matrix& res = NEWM(a.Cols(), a.Rows());

		for(uint i = 0; i < a.Rows(); i++)		
			for(uint j = 0; j < a.Cols(); j++)		
				res[i][j] = a[i][j] + b;

		return res;
	}

	Matrix& Add(Matrix& a, Matrix& b){

		assert(a.Cols() == b.Rows());

		Matrix& res = NEWM(a.Cols(), a.Rows());

		for(uint i = 0; i < a.Rows(); i++)		
			for(uint j = 0; j < a.Cols(); j++)		
				res[i][j] = a[i][j] + b[i][j];

		return res;
	}

	Matrix& Sub(Matrix& a, double b){
		Matrix& res = NEWM(a.Cols(), a.Rows());

		for(uint i = 0; i < a.Rows(); i++)		
			for(uint j = 0; j < a.Cols(); j++)		
				res[i][j] = a[i][j] - b;

		return res;
	}

	Matrix& Sub(Matrix& a, Matrix& b){

		assert(a.Cols() == b.Rows());

		Matrix& res = NEWM(a.Cols(), a.Rows());

		for(uint i = 0; i < a.Rows(); i++)		
			for(uint j = 0; j < a.Cols(); j++)		
				res[i][j] = a[i][j] - b[i][j];

		return res;
	}

#pragma endregion Basic Operations

	double Det(Matrix& m){
		if(m.Cols() == 1 && m.Rows() == 1)
			return m[0][0];

		if(m.Cols() == 2 || m.Rows() == 2)
			return m[0][0] * m[1][1] - m[1][0] * m[0][1];
		
		double det = 0;
		for(uint j = 0; j < m.Cols(); j++)		
			det += m[0][j] * Cofactor(m, 0, j);	
		return det;		
	}

	double Cofactor(Matrix& m, uint i, uint j) {

		// �������������� ���������� 
		// https://upload.wikimedia.org/math/a/1/a/a1ada4409e7e964f57f4c28a8e160b0e.png

		double res = Minor(m, i, j);

		if((j + i) % 2 == 1)
			res *= -1;

		return res;
	}

	double Minor(Matrix& m, uint row, uint col) {

		// ����� �������
		// https://en.wikipedia.org/wiki/Minor_(linear_algebra)

		Matrix& slice = Slice(m, row, col);
		double det = Det(slice);		
		return det;
	}

	void Transpose(Matrix& m) {

		// ���������������� �������

		Matrix& copy = Copy(m);
		m.Resize(copy.Rows(), copy.Cols());

		for(uint i = 0; i < m.Cols(); i++)
			for(uint j = 0; j < m.Rows(); j++)		
				m[j][i] = copy[i][j];		
	}	
	
	Matrix& Slice(Matrix& m, uint row, uint col)
	{
		assert(m.Cols() > 1 && m.Rows() > 1);

		// New indexes
		uint ni = 0;
		uint nj = 0;
		Matrix& res = NEWM(m.Cols() - 1, m.Rows() - 1);

		for(uint i = 0; i < m.Rows(); i++)	
		{
			if(i == row) continue;
			nj = 0;
			for(uint j = 0; j < m.Cols(); j++)
			{				
				if(j == col) continue;	
				res[ni][nj] = m[i][j];
				nj++;
			}
			ni++;
		}

		return res;
	}

	void SwapRows(Matrix& m, uint row1, uint row2){
		uint j = 0;
		double tmp;

		for(; j < m.Cols(); j++)
		{
			tmp = m[row1][j];
			m[row1][j] = m[row2][j];
			m[row2][j] = tmp;
		}
	}

	void SwapCols(Matrix& m, uint col1, uint col2){
		uint i = 0;
		double tmp;

		for(; i < m.Rows(); i++)
		{
			tmp = m[i][col1];
			m[i][col1] = m[i][col2];
			m[i][col2] = tmp;
		}
	}	

	void SumRows(Matrix& m, uint from, uint to, double k /* = 1 */) {
		uint j = 0;

		for(; j < m.Cols() ; j++)	
			m[to][j] += m[from][j] * k;	
	}

	void SumCols(Matrix& m, uint from, uint to, double k /* = 1 */){
		uint i = 0;

		for(; i < m.Rows() ; i++)	
			m[i][to] += m[i][from] * k;	
	}

	void MultiplyRow(Matrix& m, uint i, double k){
		for(uint j = 0; j < m.Cols(); j++)
			m[i][j] *= k;
	}

	Matrix& Adjugate(Matrix& m){

		// ������� �������
		// https://en.wikipedia.org/wiki/Adjugate_matrix

		Matrix& adj = NEWM(m.Cols(), m.Rows());

		for(uint i = 0; i < m.Rows(); i++)
			for(uint j = 0; j < m.Cols(); j++)		
				adj[i][j] = Cofactor(m, i, j);

		return adj;
	}

	Matrix& Inverse(Matrix& m) {

		// ������� https://upload.wikimedia.org/math/d/0/6/d06a2a76a5bffb44747e88ac2182df4f.png
		return Div(Adjugate(m), Det(m));
	}

	uint Rang(Matrix& m){

		// ������� ����� �������
		// http://deadbeef.narod.ru/work/articles/mrang/index.htm

		Matrix t = Triangular(m);
		uint iterations = MIN(t.Cols(), t.Rows());
		uint rang = 0;

		for(rang = 0; rang < iterations; rang++)	
			if(t[rang][rang] != 1)
				break;

		if(rang == 0)
			rang = 1;

		return rang;
	}

	Matrix& Triangular(Matrix& m){

		// �������������� ������� � ������������ ����

		Matrix& tmp = Copy(m);
		uint iterations = MIN(tmp.Cols(), tmp.Rows());

		for(uint it = 0; it < iterations; it++)
		{
			uint cur_row;
			uint cur_col;

			if(tmp[it][it] == 0)
			{
				bool found = 0;
				uint i2 = it;
				uint j2 = it;

				for(; i2 < tmp.Rows(); i2++) {
					for(; j2 < tmp.Rows(); j2++) {
						if(tmp[i2][j2] != 0) {
							found = true;
							break;
						}
					}
				}

				if(!found)
					break;
				else
				{
					SwapRows(tmp,it,i2);
					SwapCols(tmp,it,j2);
				}
			}

			MultiplyRow(tmp, it, 1 / tmp[it][it]);

			for(cur_col = it + 1; cur_col < tmp.Cols(); cur_col++)		
				SumCols(tmp, it, cur_col, (-1) * tmp[it][cur_col] / tmp[it][it]);	

			for(cur_row = it + 1; cur_row < tmp.Rows(); cur_row++)		
				SumRows(tmp, it, cur_row, (-1) * tmp[cur_row][it] / tmp[it][it]);			
		}

		return tmp;
	}
}
